package templates

class PullRequestTemplate {
 static void create(pipelineJob, config) {
  pipelineJob.with {
   description("This job has been created programatically.")
   definition {
    cpsScm {
     scm {
      git {
       remote {
        url(config.repo)
       }
       branch("master")
      }
     }
     triggers {
      gitlabPush {
       buildOnPushEvents(true)
      }
     }
     scriptPath("Jenkinsfile")
    }
   }
  }
 }
}