# Jenkins DSL YAML

#### Usage

Create a `.yml` file under the `configs` directory with the following structure, but replace with your project information:

```
project: Go Docker  <br>
repo: https://gitlab.com/dblore/jenkins-dsl-go  <br>
email: dan.blore@exceptionuk.com
```

The script will read all files ending in `.yml` in the folder. It will also read multiple configurations in one file when seperated by a line space, which could be useful for pipelines relating to the same project.

#### Configuring the seed job

This repository has to be set up manually in Jenkins as a Freestyle Job.

Add the repository link to the *Source Code Management* section of the job.

Under *Build* select the Execute Shell build step and paste the following:

```
#!/bin/bash
mkdir -p libs && cd libs

if [ ! -f "snakeyaml-1.25.jar" ]; then
    wget https://repo1.maven.org/maven2/org/yaml/snakeyaml/1.25/snakeyaml-1.25.jar
fi
```

This will pull the SnakeYAML jar file from the mvnrepositroy and put it in the `libs` folder in the workspace.

This is required for the Groovy script to read the YAML configuration file.

Add another buil step called *Process Job DSLs* _(Note: The Job DSL plugin is required on Jenkins.)_

Select `Look on Filesystem` and put `main.groovy` as the DSL Scripts path.

Click advanced and input `libs/*.jar` into the *Additional classpath* field.

For this to be visisble, you must disable DSL Script Security. This can be found in `Manage Jenkins > Configure Global Security`. This is not recommended if your Jenkins is open to the public, as it allows scripts to be ran directly on the Jenkins master box. However if the instance is locked down and access is controlled, this shouldn't be a problem. Currently looking for a workaround to not disable Job DSL Script security but this is the way for now.

#### _TODO:_

- Configure the script to support multibranch pipelines wherever a Jenkinsfile is present, as it's currently only supporting declared branches.

- Configure build triggers as it's currently manual.